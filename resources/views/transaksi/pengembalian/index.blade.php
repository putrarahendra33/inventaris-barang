@extends('layout.default')
@section('content')
@section('title', $title)
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@yield('title')</h2>
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Karyawan</th>
                                        <th>Nama Petugas</th>
                                        <th>Label Barang</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Jumlah Kembali</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Karyawan</th>
                                        <th>Nama Petugas</th>
                                        <th>Label Barang</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Jumlah Kembali</th>
                                        <th>Opsi</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($data as $data)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data->merk }}</td>
                                        <td>{{ $data->model }}</td>
                                        <td>{{ $data->label_alat }}</td>
                                        <td>{{ $data->jenis_alat }}</td>
                                        <td>{{ $data->nm_pembuat }}</td>
                                        <td><a href="{{ url('/daftar/inventaris/edit/'.$data->kd_alat) }}"><i class="large material-icons">mode_edit</i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
            
@stop