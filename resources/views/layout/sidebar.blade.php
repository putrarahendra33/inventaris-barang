<!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{ url('/images/user.png')}}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->nm_petugas }}</div>
                    <div class="email">{{ Auth::user()->hak_akses }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ url('/logout') }}"><i class="material-icons">input</i>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="{{ url('/')}}">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    @if(Auth::user()->hak_akses == 'petugas')
                    <li>    
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Transaksi</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ url('/transaksi/peminjaman') }}">Peminjaman Barang</a>
                            </li>
                            <li>
                                <a href="{{ url('/transaksi/pengembalian') }}">Pengembalian Barang</a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="header">Administrator</li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">pie_chart</i>
                            <span>List Barang</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ url('/daftar/inventaris') }}">Daftar Barang</a>
                            </li>
                            <li>
                                <a href="{{ url('/daftar/pembuat') }}">Daftar Pembuat</a>
                            </li>
                            <li>
                                <a href="{{ url('/daftar/kategori') }}">Daftar Kategori</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">content_copy</i>
                            <span>List Pengguna</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ url('/daftar/karyawan') }}">Daftar Karyawan</a>
                            </li>
                            <li>
                                <a href="{{ url('/daftar/petugas') }}">Daftar Petugas</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                @endif
            </div>
            <!-- #Menu -->