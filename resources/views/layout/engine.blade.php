<!-- Engine -->
@if(Auth::user()->hak_akses == 'petugas')
<body class="theme-indigo">
<!-- Page Loader --> 
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-black">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Welcome {{ Auth::user()->nm_petugas }}</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
@else(Auth::user()->hak_akses == 'pimpinan')
<body class="theme-teal">
<!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-black">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Welcome {{ Auth::user()->nm_petugas }}</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
@endif


    
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->