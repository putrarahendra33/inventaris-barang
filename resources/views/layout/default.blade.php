<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title')</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ url('/favicon.ico')}}">

    <!-- Google Fonts -->
    {{ Html::style('/css/fonts/roboto.css')}}
    {{ Html::style('/css/fonts/material.css')}}

    <!-- Bootstrap Core Css -->
    {{ Html::style('/plugins/bootstrap/css/bootstrap.css')}}

    <!-- Bootstrap Tagsinput Css -->
    {{ Html::style('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}

    <!-- Waves Effect Css -->
    {{ Html::style('/plugins/node-waves/waves.css')}}

    <!-- JQuery DataTable Css -->
    {{ Html::style('/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
    
    <!-- Bootstrap Select Css -->
    {{ Html::style('/plugins/bootstrap-select/css/bootstrap-select.css')}}

    <!-- Animation Css -->
    {{ Html::style('/plugins/animate-css/animate.css')}}

    <!-- Morris Chart Css-->
    {{ Html::style('/plugins/morrisjs/morris.css')}}

    <!-- Custom Css -->
    {{ Html::style('/css/style.css')}}

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    {{ Html::style('/css/themes/all-themes.css')}}
</head>
<!-- Theme Engine -->
@include('layout.engine')
    
    @include('layout.header')
    <section>

        @include('layout.sidebar')

        @include('layout.footer')

    @yield('content')

    <!-- Jquery Core Js -->
    {{ Html::script('/plugins/jquery/jquery.min.js')}}

    <!-- Bootstrap Core Js -->
    {{ Html::script('/plugins/bootstrap/js/bootstrap.js')}}

    <!-- Select Plugin Js -->
    {{ Html::script('/plugins/bootstrap-select/js/bootstrap-select.js')}}

    <!-- Slimscroll Plugin Js -->
    {{ Html::script('/plugins/jquery-slimscroll/jquery.slimscroll.js')}}

    <!-- Waves Effect Plugin Js -->
    {{ Html::script('/plugins/node-waves/waves.js')}}

    <!-- Jquery CountTo Plugin Js -->
    {{ Html::script('/plugins/jquery-countto/jquery.countTo.js')}}

    <!-- Morris Plugin Js -->
    {{ Html::script('/plugins/raphael/raphael.min.js')}}
    {{ Html::script('/plugins/morrisjs/morris.js')}}

    <!-- ChartJs -->
    {{ Html::script('/plugins/chartjs/Chart.bundle.js')}}

    <!-- Flot Charts Plugin Js -->
    {{ Html::script('/plugins/flot-charts/jquery.flot.js')}}
    {{ Html::script('/plugins/flot-charts/jquery.flot.resize.js')}}
    {{ Html::script('/plugins/flot-charts/jquery.flot.pie.js')}}
    {{ Html::script('/plugins/flot-charts/jquery.flot.categories.js')}}
    {{ Html::script('/plugins/flot-charts/jquery.flot.time.js')}}

    <!-- Sparkline Chart Plugin Js -->
    {{ Html::script('/plugins/jquery-sparkline/jquery.sparkline.js')}}
    <!-- Jquery DataTable Plugin Js -->
    {{ Html::script('/plugins/jquery-datatable/jquery.dataTables.js')}}
    {{ Html::script('/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}
    {{ Html::script('/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}
    {{ Html::script('/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}
    {{ Html::script('/plugins/jquery-datatable/extensions/export/jszip.min.js')}}
    {{ Html::script('/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}
    {{ Html::script('/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}
    {{ Html::script('/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}
    {{ Html::script('/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}

    <!-- Input Mask Plugin Js -->
    {{ Html::script('/plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}

    <!-- Custom Js -->
    {{ Html::script('/js/pages/tables/jquery-datatable.js')}}
    {{ Html::script('/js/admin.js')}}
    {{ Html::script('/js/pages/index.js')}}
    {{ Html::script('/js/pages/forms/advanced-form-elements.js')}}

    <!-- Demo Js -->
    {{ Html::script('/js/demo.js')}}
</body>

</html>