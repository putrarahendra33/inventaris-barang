@extends('layout.default')
@section('content')
@section('title', $title)
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@yield('title')</h2>
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <a href="{{ url('/daftar/inventaris/tambah')}}" class="btn btn-sm btn-primary waves-effect"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
                        </div>
                        <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Merk</th>
                                        <th>Model</th>
                                        <th>Label</th>
                                        <th>Kategori</th>
                                        <th>Pembuatan</th>
                                        <th>Tahun Pembuatan</th>
                                        <th>Kondisi</th>
                                        <th>Status</th>
                                        <th>Jumlah Stok</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Merk</th>
                                        <th>Model</th>
                                        <th>Label</th>
                                        <th>Kategori</th>
                                        <th>Pembuatan</th>
                                        <th>Tahun Pembuatan</th>
                                        <th>Kondisi</th>
                                        <th>Status</th>
                                        <th>Jumlah Stok</th>
                                        <th>Opsi</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($data as $data)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data->merk }}</td>
                                        <td>{{ $data->model }}</td>
                                        <td>{{ $data->label_alat }}</td>
                                        <td>{{ $data->jenis_alat }}</td>
                                        <td>{{ $data->nm_pembuat }}</td>
                                        <td>{{ $data->thn_buat }}</td>
                                        <td>{{ ($data->kondisi_brg == 'baik') ? 'Baik' : 'Rusak' }}</td>
                                        <td>{{ ($data->status_brg == 'tersedia') ? 'Tersedia' : 'Kosong' }}</td>
                                        <td>{{ $data->stok }}
                                        <td><a href="{{ url('/daftar/inventaris/edit/'.$data->kd_alat) }}"><i class="large material-icons">mode_edit</i></a>
                                        <a href="{{ url('/daftar/inventaris/hapus/'.$data->kd_alat  ) }}"><i class="large material-icons">delete</i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
            
@stop