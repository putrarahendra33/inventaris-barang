@extends('layout.default')
@section('content')
@section('title', $title)
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@yield('title')</h2>
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card"> 
                        <div class="body">
                        {{ Form::open(
                            [
                                'method' => 'POST'

                            ])  
                        }}
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Label Alat</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="label_alat" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Label Alat">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Model</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="model" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Model">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jk">Kategori:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            {{ Form::select('kd_kategori', $list_kategori) }}
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Merk</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="merk" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Merk">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jk">Pembuat:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            {{ Form::select('kd_pembuat', $list_pembuat) }}
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Tahun Pembuatan</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" name="thn_buat" maxlength="4" minlength="3" required class="form-control" placeholder="Masukan Tahun">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jk">Kondisi Barang:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            {{ Form::select('kondisi_brg',['baik' => 'Baik', 'rusak' => 'Rusak']) }}
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Jumlah Stok</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" name="stok" maxlength="4" minlength="3" required class="form-control" placeholder="Masukan Jumlah Stok">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                            <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">TAMBAH</button>
                                    </div>
                                </div>
                            {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
            
@stop