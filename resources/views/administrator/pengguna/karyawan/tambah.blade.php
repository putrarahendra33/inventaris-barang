@extends('layout.default')
@section('content')
@section('title', $title)
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@yield('title')</h2>
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card"> 
                        <div class="body">
                        {{ Form::open(
                            [
                                'method' => 'POST'

                            ])  
                        }}
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Nama Lengkap</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="nm_karyawan" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Nama">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Status</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="status" maxlength="255" minlength="3" required class="form-control" placeholder="Status Karyawan">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Ruang</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="ruang" maxlength="255" minlength="3" required class="form-control" placeholder="Ruang Karyawan">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jk">Jenis Kelamin:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            {{ Form::select('j_kel', ['cowok' => 'Pria', 'cewek' => 'Wanita']) }}
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Alamat</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="almt_karyawan" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Alamat">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">No. Telepon</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="tlp_karyawan" maxlength="12" minlength="3" required class="form-control mobile-phone-number" placeholder="Masukan No. Telepon">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                            <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">TAMBAH</button>
                                    </div>
                                </div>
                            {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
            
@stop