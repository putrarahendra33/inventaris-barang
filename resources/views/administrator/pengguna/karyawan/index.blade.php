@extends('layout.default')
@section('content')
@section('title', $title)
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@yield('title')</h2>
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <a href="{{ url('/daftar/karyawan/tambah')}}" class="btn btn-sm btn-primary waves-effect"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
                        </div>
                        <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Lengkap</th>
                                        <th>Status</th>
                                        <th>Ruang</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat Lengkap</th>
                                        <th>No. Telepon</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Lengkap</th>
                                        <th>Status</th>
                                        <th>Ruang</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat Lengkap</th>
                                        <th>No. Telepon</th>
                                        <th>Opsi</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($data as $data)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data->nm_karyawan }}</td>
                                        <td>{{ $data->status }}</td>
                                        <td>{{ $data->ruang }}</td>
                                        <td>{{ ($data->j_kel == 'cowok') ? 'Pria' : 'Wanita' }}</td>
                                        <td>{{ $data->almt_karyawan }}</td>
                                        <td>{{ $data->tlp_karyawan }}</td>
                                        <td><a href="{{ url('/daftar/karyawan/edit/'.$data->kd_karyawan) }}"><i class="large material-icons">mode_edit</i></a>
                                        <a href="{{ url('/daftar/karyawan/hapus/'.$data->kd_karyawan) }}"><i class="large material-icons">delete</i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
            
@stop