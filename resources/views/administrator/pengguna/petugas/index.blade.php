@extends('layout.default')
@section('content')
@section('title', $title)
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@yield('title')</h2>
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <a href="{{ url('/daftar/petugas/tambah')}}" class="btn btn-sm btn-primary waves-effect"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
                        </div>
                        <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat</th>
                                        <th>Telepon</th>
                                        <th>Hak Akses</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Alamat</th>
                                        <th>Telepon</th>
                                        <th>Hak Akses</th>
                                        <th>Opsi</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($data as $data)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $data->nm_petugas }}</td>
                                        <td>{{ ($data->j_kel_petugas == 'cowok') ? 'Pria' : 'Wanita' }}</td>
                                        <td>{{ $data->almt_petugas }}</td>
                                        <td>{{ $data->tlp_petugas }}</td>
                                        <td>{{ ($data->hak_akses == 'pimpinan') ? 'Pimpinan' : 'Petugas' }}</td>
                                        <td><a href="{{ url('/daftar/petugas/edit/'.$data->kd_petugas) }}"><i class="large material-icons">mode_edit</i></a>
                                        <a href="{{ url('/daftar/petugas/hapus/'.$data->kd_petugas) }}"><i class="large material-icons">delete</i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
            
@stop