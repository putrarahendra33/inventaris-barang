@extends('layout.default')
@section('title', $title)
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@yield('title')</h2>
            </div>
            
            <!-- Tabs With Icon Title -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                @yield('title')
                            </h2>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#profile" data-toggle="tab">
                                        <i class="material-icons">face</i> PROFILE
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#akun" data-toggle="tab">
                                        <i class="material-icons">settings</i> SETTINGS
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Profile Edit -->
                                <div role="tabpanel" class="tab-pane fade in active" id="profile">
                                {{ Form::open(
                                    [
                                        'method' => 'POST'
                                    ]) 
                                }}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Nama Lengkap</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="nm_petugas" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Nama" value="{{ $data->nm_petugas }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jk">Jenis Kelamin</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            {{ Form::select('j_kel_petugas', ['cowok' => 'Pria', 'cewek' => 'Wanita'], $data->j_kel_petugas) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Alamat</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="almt_petugas" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Alamat" value="{{ $data->almt_petugas }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">No. Telepon</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="tlp_petugas" maxlength="12" minlength="3" required class="form-control mobile-phone-number" placeholder="Masukan No. Telepon" value="{{ $data->tlp_petugas }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">SIMPAN</button>
                                    </div>
                                </div>
                                {{ Form::close() }}
                                
                                <!-- Akun Edit -->
                                <div role="tabpanel" class="tab-pane fade" id="akun">
                                {{ Form::open(
                                    [
                                        'method' => 'POST',
                                        'action' => ['MainController@update_petugas', $data->kd_petugas]
                                    ]) 
                                }}
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Password Lama</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="password" name="password_old" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Password Baru</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="password" name="password" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Konfirmasi Password</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="password" name="password_confirmation" maxlength="255" minlength="3" required class="form-control" placeholder="Masukan Password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SIMPAN</button>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tabs With Icon Title -->
        </div>
    </section>

@stop
