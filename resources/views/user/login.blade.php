<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ url('/favicon.ico')}}">

    <!-- Google Fonts -->
    {{ Html::style('/css/fonts/roboto.css')}}
    {{ Html::style('/css/fonts/material.css')}}

    <!-- Bootstrap Core Css -->
    {{ Html::style('/plugins/bootstrap/css/bootstrap.css')}}

    <!-- Waves Effect Css -->
    {{ Html::style('/plugins/node-waves/waves.css')}}

    <!-- Animation Css -->
    {{ Html::style('/plugins/animate-css/animate.css')}}

    <!-- Custom Css -->
    {{ Html::style('/css/style.css')}}
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">Admin<b>BSB</b></a>
            <small>Admin BootStrap Based - Material Design</small>
        </div>
        <div class="card">
            <div class="body">
                
                {{ Form::open(['method' => 'POST', 'id' => 'sign_in', 'action' => 'MainController@doLogin']) }}
                    
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="user" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="sign-up.html">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot-password.html">Forgot Password?</a>
                        </div>
                    </div>
                
                {{ Form::close() }}
            
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    {{ Html::script('/plugins/jquery/jquery.min.js')}}

    <!-- Bootstrap Core Js -->
    {{ Html::script('/plugins/bootstrap/js/bootstrap.js')}}

    <!-- Waves Effect Plugin Js -->
    {{ Html::script('/plugins/node-waves/waves.js')}}

    <!-- Validation Plugin Js -->
    {{ Html::script('/plugins/jquery-validation/jquery.validate.js')}}

    <!-- Custom Js -->
    {{ Html::script('/js/admin.js')}}
    {{ Html::script('/js/pages/examples/sign-in.js')}}
</body>

</html>