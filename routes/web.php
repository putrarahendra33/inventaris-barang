<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| RIP
| ROUTE
| SAD
| LIFE
*/
/* Middleware Default "WEB" : memeriksa session */
Route::group(['middleware' => 'web'], function(){
	/* Login */
	Route::get('/login', 'MainController@login')->middleware('guest');
	Route::post('/login', 'MainController@doLogin');
	/* Logout */
	Route::get('/logout', 'MainController@doLogout');
});

/* Diakses saat login */
Route::group(['middleware' => ['web','auth','pimpinan']], function()
	{
		Route::get('/', 'IndexController@index');

	});

/* Akses Petugas */
Route::group(['middleware' => ['web','auth','petugas']], function(){

	/* Transaksi */
	
	Route::get('/peminjaman', 'IndexController@peminjaman');

	/* Pengguna */

		/** Petugas */

		Route::get('/daftar/petugas', 'MainController@daftar_petugas');
		Route::get('/daftar/petugas/tambah', 'MainController@tambah_petugas');
		Route::post('/daftar/petugas/tambah', 'MainController@doTambah_petugas');
		Route::get('/daftar/petugas/edit/{id}', 'MainController@edit_petugas');
		Route::post('/daftar/petugas/edit/{id}', 'MainController@doEdit_petugas');
		Route::post('/daftar/petugas/editSec/{id}', 'MainController@update_petugas');
		Route::get('/daftar/petugas/hapus/{id}', 'MainController@hapus_petugas');
		/** Karyawan */

		Route::get('/daftar/karyawan', 'MainController@daftar_karyawan');
		Route::get('/daftar/karyawan/tambah', 'MainController@tambah_karyawan');
		Route::post('/daftar/karyawan/tambah', 'MainController@doTambah_karyawan');
		Route::get('/daftar/karyawan/edit/{id}', 'MainController@edit_karyawan');
		Route::post('/daftar/karyawan/edit/{id}', 'MainController@doEdit_karyawan');
		Route::get('/daftar/karyawan/hapus/{id}', 'MainController@hapus_karyawan');

	/* Inventaris */

		/** Barang */
		Route::get('/daftar/inventaris', 'InventarisController@daftar_inventaris');
		Route::get('/daftar/inventaris/tambah', 'InventarisController@tambah_inventaris');
		Route::post('/daftar/inventaris/tambah', 'InventarisController@doTambah_inventaris');
		Route::get('/daftar/inventaris/edit/{id}', 'InventarisController@edit_inventaris');
		Route::post('/daftar/inventaris/edit/{id}', 'InventarisController@doEdit_inventaris');
		Route::post('/daftar/inventaris/editSec/{id}', 'InventarisController@update_inventaris');
		Route::get('/daftar/inventaris/hapus/{id}', 'InventarisController@hapus_inventaris');

		/** Pembuat */
		Route::get('/daftar/pembuat', 'InventarisController@daftar_pembuat');
		Route::get('/daftar/pembuat/tambah', 'InventarisController@tambah_pembuat');
		Route::post('/daftar/pembuat/tambah', 'InventarisController@doTambah_pembuat');
		Route::get('/daftar/pembuat/edit/{id}', 'InventarisController@edit_pembuat');
		Route::post('/daftar/pembuat/edit/{id}', 'InventarisController@doEdit_pembuat');
		Route::post('/daftar/pembuat/editSec/{id}', 'InventarisController@update_pembuat');
		Route::get('/daftar/pembuat/hapus/{id}', 'InventarisController@hapus_pembuat');

		/** Kategori */
		Route::get('/daftar/kategori', 'InventarisController@daftar_kategori');
		Route::get('/daftar/kategori/tambah', 'InventarisController@tambah_kategori');
		Route::post('/daftar/kategori/tambah', 'InventarisController@doTambah_kategori');
		Route::get('/daftar/kategori/edit/{id}', 'InventarisController@edit_kategori');
		Route::post('/daftar/kategori/edit/{id}', 'InventarisController@doEdit_kategori');
		Route::post('/daftar/kategori/editSec/{id}', 'InventarisController@update_kategori');
		Route::get('/daftar/kategori/hapus/{id}', 'InventarisController@hapus_kategori');

	/* Transaksi */

		/** Peminjaman */
		Route::get('/transaksi/peminjaman', 'TransaksiController@daftar_peminjaman');
		Route::get('/transaksi/peminjaman/tambah', 'TransaksiController@tambah_peminjaman');
		Route::post('/transaksi/peminjaman/tambah', 'TransaksiController@doTambah_peminjaman');

		/** Pengembalian */
		Route::get('/transaksi/pengembalian', 'TransaksiController@daftar_pengembalian');
		Route::get('/transaksi/pengembalian/tambah', 'TransaksiController@tambah_pengembalian');
		Route::post('/transaksi/pengembalian/tambah', 'TransaksiController@doTambah_pengembalian');

});

/* 
Route::get('/login', 'MainController@login');

Route::get('/login', 'MainController@login');
Route::post('/login', 'MainController@doLogin');
Route::get('/logout', 'MainController@doLogout');

Route::get('/', 'IndexController@index');
Route::resource('/petugas', 'PetugasController');
*/

Route::get('/kode', function() {
    echo Hash::make('admin');
});
