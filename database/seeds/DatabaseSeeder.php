<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('petugas')->insert(
      [
       	'nm_petugas' => 'Rahendra Putra',
       	'j_kel_petugas' => 'cowok',
       	'almt_petugas' => 'Sukodono',
       	'tlp_petugas' => '085731806147',
       	'user' => 'admin',
       	'password' => bcrypt('admin'),
        'hak_akses' => 'petugas',
      ]);

      DB::table('petugas')->insert(
      [
        'nm_petugas' => 'Pimpinan',
        'j_kel_petugas' => 'cowok',
        'almt_petugas' => 'Surabaya',
        'tlp_petugas' => '083830811072',
        'user' => 'pimpinan',
        'password' => bcrypt('pimpinan'),
        'hak_akses' => 'pimpinan',
      ]);

      DB::table('kategori')->insert(
      [
        'jenis_alat' => 'SmartPhone',
      ]);

      DB::table('pembuat')->insert(
      [
        'nm_pembuat' => 'Pak Sholeh',
        'kota_pembuat' => 'Wonogiri',
      ]);
    }
}
