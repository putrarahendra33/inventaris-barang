<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Peminjaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Peminjaman', function (Blueprint $table) {
            $table->increments('kd_inventaris');
            $table->string('kd_karyawan');
            $table->string('kd_petugas');
            $table->string('kd_alat');
            $table->integer('jumlah');
            $table->timestamp('tgl_pinjam');
            $table->enum('status', ['belum', 'sudah']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Peminjaman');
    }
}
