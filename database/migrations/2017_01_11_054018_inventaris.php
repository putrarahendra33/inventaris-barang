<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Inventaris extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventaris', function (Blueprint $table) {
            $table->increments('kd_alat');
            $table->string('model');
            $table->integer('kd_kategori');
            $table->string('merk');
            $table->integer('kd_pembuat');
            $table->integer('thn_buat');
            $table->enum('kondisi_brg', ['baik', 'rusak']);
            $table->enum('status_brg', ['tersedia', 'kosong']);
            $table->integer('stok');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventaris');
    }
}
