<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Petugas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petugas', function (Blueprint $table) {
            $table->increments('kd_petugas');
            $table->string('nm_petugas');
            $table->enum('j_kel_petugas', ['cowok', 'cewek']);
            $table->string('almt_petugas');
            $table->string('tlp_petugas');
            $table->string('user')->unique();
            $table->string('password');
            $table->enum('hak_akses', ['petugas', 'pimpinan']);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petugas');
    }
}
