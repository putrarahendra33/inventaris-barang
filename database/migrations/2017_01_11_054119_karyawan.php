<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Karyawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->increments('kd_karyawan');
            $table->string('status');
            $table->string('nm_karyawan');
            $table->string('ruang');
            $table->timestamp('tgl_daftar');
            $table->enum('j_kel', ['cowok', 'cewek']);
            $table->string('almt_karyawan');
            $table->string('tlp_karyawan');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
