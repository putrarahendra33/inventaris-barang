<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Trigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER kurangStok AFTER INSERT ON peminjaman FOR EACH ROW
        BEGIN
        UPDATE inventaris SET stok = stok - NEW.jumlah WHERE kd_alat = NEW.kd_alat;
        END');

        DB::unprepared('CREATE TRIGGER tambahStok AFTER INSERT ON pengembalian FOR EACH ROW
        BEGIN
        UPDATE peminjaman SET status = "sudah" WHERE kd_alat = NEW.kd_alat;
        UPDATE inventaris SET stok = stok + NEW.jumlah WHERE kd_alat = NEW.kd_alat;
        END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS kurangStok');
        DB::unprepared('DROP TRIGGER IF EXISTS tambahStok');
    }
}
