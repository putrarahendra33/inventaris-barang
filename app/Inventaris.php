<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventaris extends Model
{
	protected $table = 'inventaris';
    protected $primaryKey = 'kd_alat';

    public $timestamps = false;
}
