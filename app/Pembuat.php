<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembuat extends Model
{
    protected $table = 'pembuat';
    protected $primaryKey = 'kd_pembuat';

    public $timestamps = false;
}
