<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Response;
use Validator;
/* Memanggil Model Petugas */
use App\Petugas;
/* Menaggil Model Karyawan */
use App\Karyawan;

class IndexController extends Controller
{
    public function index()
    {
    	$title = 'Dashboard';

    	$content = view('welcome', 
    		[
    			'title' => $title,
    			'petugas' => Petugas::count(), 
    			'karyawan' => Karyawan::count() 
    		]);

    	return Response::make($content, 200)
    		->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
    }
}
