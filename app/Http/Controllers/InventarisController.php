<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Response;
use Validator;
use Hash;
use Auth;
/* Memanggil Model Inventaris */
use App\Inventaris;
/* Memanggil Model Pembuat */
use App\Pembuat;
/* Memanggil Model Kategori */
use App\Kategori;
/* Memanggil Model Label */
use App\Label;

class InventarisController extends Controller
{
	public function __construct()
    {
        $this->list_kategori = Kategori::pluck('jenis_alat', 'kd_kategori')->toArray();
        $this->list_pembuat = Pembuat::pluck('nm_pembuat', 'kd_pembuat')->toArray();
    }

    /* Fungsi CRUD Inventaris Start */
	public function daftar_inventaris()
	{
		$title = 'Daftar Inventaris';
        $data = Inventaris::join('label_alat', 'inventaris.kd_alat', '=', 'label_alat.kd_alat')
            ->join('kategori', 'inventaris.kd_kategori' , '=', 'kategori.kd_kategori')
            ->join('pembuat', 'inventaris.kd_pembuat', '=', 'pembuat.kd_pembuat')
            ->get();

        $contents = view('administrator.inventaris.barang.index', 
            [
                'title' => $title, 
                'no' => 1, 
                'data' => $data,
                'list_kategori' => $this->list_kategori,
                'list_pembuat' => $this->list_pembuat
            ]);

            return Response::make($contents, 200)
                ->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
	}

	public function tambah_inventaris()
	{
		$title = 'Tambah Inventaris';
		$data = Inventaris::join('label_alat', 'inventaris.kd_alat', '=', 'label_alat.kd_alat')
            ->join('kategori', 'inventaris.kd_kategori' , '=', 'kategori.kd_kategori')
            ->join('pembuat', 'inventaris.kd_pembuat', '=', 'pembuat.kd_pembuat')
            ->get();

		$contents = view('administrator.inventaris.barang.tambah',
			[
				'title' => $title,
				'data' => $data,
				'list_kategori' => $this->list_kategori,
                'list_pembuat' => $this->list_pembuat
			]);

		return Response::make($contents, 200)
			->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
	}

	public function doTambah_inventaris(Request $request)
	{
		$valid = Validator::make($request->all(), [
            'label_alat' => 'required',
            'model' => 'required',
            'kd_kategori' => 'required',
            'merk' => 'required',
            'kd_pembuat' => 'required',
            'thn_buat' => 'required',
            'kondisi_brg' => 'required',
            'stok' => 'required'
        ]);

		$label = new Label();
        $inventaris = new Inventaris();

        if ($valid->passes())
        {
            $inventaris->model = request()->model;
            $inventaris->kd_kategori = request()->kd_kategori;
            $inventaris->merk = request()->merk;
            $inventaris->kd_pembuat = request()->kd_pembuat;
            $inventaris->thn_buat = request()->thn_buat;
            $inventaris->kondisi_brg = request()->kondisi_brg;
            $inventaris->stok = request()->stok;
            $inventaris->save();

            $label->label_alat = request()->label_alat;
            $label->save(); 

            return redirect('/daftar/inventaris');
        }
        else
        {
            return redirect('/daftar/inventaris/gagal');
        }
	}

    public function edit_inventaris($id)
    {
        $data = Inventaris::where('inventaris.kd_alat', '=', $id)
                ->join('label_alat', 'inventaris.kd_alat', '=', 'label_alat.kd_alat')
                ->first();

        if ($data)
        {
            return view('administrator.inventaris.barang.edit', 
                [
                    'title' => 'Edit Inventaris', 
                    'data' => $data,
                    'list_kategori' => $this->list_kategori,
                    'list_pembuat' => $this->list_pembuat
                ]);
        }
        else
        {
            return redirect('/daftar/inventaris');
        }
    }

    public function doEdit_inventaris($id)
    {
        $valid = Validator::make(request()->all(), [
            'label_alat' => 'required',
            'model' => 'required',
            'kd_kategori' => 'required',
            'merk' => 'required',
            'kd_pembuat' => 'required',
            'thn_buat' => 'required',
            'kondisi_brg' => 'required',
            'stok' => 'required'
        ]);

        $label = new Label();
        $inventaris = new Inventaris();

        if ($valid->passes())
        {
            $label = Label::findOrFail($id);
            $label->label_alat = request()->label_alat;
            $label->update();

            $inventaris = Inventaris::findOrFail($id);
            $inventaris->model = request()->model;
            $inventaris->kd_kategori = request()->kd_kategori;
            $inventaris->merk = request()->merk;
            $inventaris->kd_pembuat = request()->kd_pembuat;
            $inventaris->thn_buat = request()->thn_buat;
            $inventaris->kondisi_brg = request()->kondisi_brg;
            $inventaris->stok = request()->stok;
            $inventaris->update();

            return redirect('/daftar/inventaris');
        }
        else
        {
            return redirect('/daftar/inventaris/gagal');
        }
    }

    public function hapus_inventaris($id)
    {
        try
        {
            $inventaris = Inventaris::findOrFail($id);
            $inventaris->delete();

            $label = Label::findOrFail($id);
            $label->delete();

            return redirect('/daftar/inventaris');
        }
        catch (ModelNotFoundException $e)
        {
            return redirect()->back();
        }
    }
	/* Fungsi CRUD Inventaris End */

	/* Fungsi CRUD Pembuat Start */
	public function daftar_pembuat()
	{
		$title = 'Daftar Pembuat';
        $data = Pembuat::get();

        $contents = view('administrator.inventaris.pembuat.index', 
        	[
        		'title' => $title, 
        		'no' => 1, 
        		'data' => $data
        	]);

            return Response::make($contents, 200)
                ->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
	}

	public function tambah_pembuat()
	{
		$title = 'Tambah Pembuat';
		$data = Pembuat::get();

		$contents = view('administrator.inventaris.pembuat.tambah',
			[
				'title' => $title,
				'data' => $data
			]);

		return Response::make($contents, 200)
			->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
	}

	public function doTambah_pembuat(Request $request)
	{
		$valid = Validator::make($request->all(), [
            'nm_pembuat' => 'required',
            'kota_pembuat' => 'required'
        ]);

        $pembuat = new Pembuat();

        if ($valid->passes())
        {
            $pembuat->nm_pembuat = request()->nm_pembuat;
            $pembuat->kota_pembuat = request()->kota_pembuat;
            $pembuat->save();

            return redirect('/daftar/pembuat');
        }
        else
        {
            return redirect('/daftar/pembuat');
        }
	}

	public function edit_pembuat($id)
	{
		try
        {
            $data = Pembuat::findOrFail($id);

            return view('administrator.inventaris.pembuat.edit', ['title' => 'Edit Pembuat', 'data' => $data]);
        }
        catch (ModelNotFoundException $e)
        {
            return redirect('/daftar/pembuat');
        }
	}

	public function doEdit_pembuat($id)
	{
		$valid = Validator::make(request()->all(), [
                'nm_pembuat' => 'required',
                'kota_pembuat' => 'required'
            ]);

        $pembuat = Pembuat::findOrFail($id);

            if ($valid->passes()) 
            {
                $pembuat->nm_pembuat = request()->nm_pembuat;
                $pembuat->kota_pembuat = request()->kota_pembuat;
                $pembuat->update();

                return redirect('/daftar/pembuat');
            }
            else
            {
                return redirect('/daftar/pembuat/');
            }
	}

	public function hapus_pembuat($id)
	{
		try
        {
            $pembuat = Pembuat::findOrFail($id);
            $pembuat->delete();

            return redirect('/daftar/pembuat');
        }
        catch (ModelNotFoundException $e)
        {
            return redirect()->back();
        }
	}
    /* Fungsi CRUD Pembuat End */

    /* Fungsi CRUD Kategori Start */
    public function daftar_kategori()
	{
		$title = 'Daftar Kategori';
        $data = Kategori::get();

        $contents = view('administrator.inventaris.kategori.index', ['title' => $title, 'no' => 1, 'data' => $data]);

            return Response::make($contents, 200)
                ->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
	}

	public function tambah_kategori()
	{
		$title = 'Tambah Kategori';
		$data = Kategori::get();

		$contents = view('administrator.inventaris.kategori.tambah',
			[
				'title' => $title,
				'data' => $data
			]);

		return Response::make($contents, 200)
			->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
	}

	public function doTambah_kategori(Request $request)
	{
		$valid = Validator::make($request->all(), [
            'jenis_alat' => 'required'
        ]);

        $kategori = new Kategori();

        if ($valid->passes())
        {
            $kategori->jenis_alat = request()->jenis_alat;
            $kategori->save();

            return redirect('/daftar/kategori');
        }
        else
        {
            return redirect('/daftar/kategori');
        }
	}

	public function edit_kategori($id)
	{
		try
        {
            $data = Kategori::findOrFail($id);

            return view('administrator.inventaris.kategori.edit', ['title' => 'Edit Kategori', 'data' => $data]);
        }
        catch (ModelNotFoundException $e)
        {
            return redirect('/daftar/kategori');
        }
	}

	public function doEdit_kategori($id)
	{
		$valid = Validator::make(request()->all(), [
                'jenis_alat' => 'required'
            ]);

        $kategori = Kategori::findOrFail($id);

            if ($valid->passes()) 
            {
                $kategori->jenis_alat = request()->jenis_alat;
                $kategori->update();

                return redirect('/daftar/kategori');
            }
            else
            {
                return redirect('/daftar/kategori/');
            }
	}

	public function hapus_kategori($id)
	{
		try
        {
            $kategori = Kategori::findOrFail($id);
            $kategori->delete();

            return redirect('/daftar/kategori');
        }
        catch (ModelNotFoundException $e)
        {
            return redirect()->back();
        }
	}
    /* Fungsi CRUD Kategori End */
}
