<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Response;
use Validator;
use Hash;
use Auth;
/* Memanggil Model Petugas */
use App\Petugas;
/* Menaggil Model Karyawan */
use App\Karyawan;

class MainController extends Controller
{
	/* Fungsi Auth Start */
    public function login()
    {
    	$content = view('user.login');

    	return Response::make($content, 200)
    		->header('Cache-control','nocache, no-store, max-age=0, must-revalidate');
    	/* 
    	if (Auth::check()){
    		return redirect('/beranda');
    	} else {
    		return view('user.login');
    	} */
    }

    public function doLogin(Request $request)
    {
    	$user = $request->input('user');
    	$password = $request->input('password');

    	$valid = Validator::make($request->all(),
    		array(
    			'user' => 'required',
    			'password' => 'required'
    		));

    	if ($valid->passes())
    	{
    		if (Auth::attempt(['user' => $user, 'password' => $password]))
    		{
    			return redirect('/');
    		}
    		else
    		{
    			return redirect()->back();
    		}
    	}
    	else
    	{
    		return redirect('/gagal');
    	}
    }

    public function doLogout()
    {
    	Auth::logout();
    	return redirect('/login');
    }

   	/* Fungsi Auth End */

    /* Fungsi CRUD Petugas Start */
    public function daftar_petugas()
    {
    	$title = 'Daftar Petugas';
    	$data = Petugas::get();

    	$contents = view('administrator.pengguna.petugas.index', ['title' => $title, 'no' => 1, 'data' => $data]);

        	return Response::make($contents, 200)
        		->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
    }

    public function tambah_petugas()
    {
        $title = 'Tambah Petugas';
        $data = Petugas::get();
        
        $contents = view('administrator.pengguna.petugas.tambah', ['title' => $title, 'data' => $data]);
        
        return Response::make($contents, 200)
            ->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
    }

    public function doTambah_petugas(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'nm_petugas' => 'required',
            'j_kel_petugas' => 'required',
            'almt_petugas' => 'required',
            'tlp_petugas' => 'required|min:11|numeric',
            'user' => 'required',
            'password' => 'required',
            'hak_akses' => 'required'
        ]);

        $admin = new Petugas();

        if ($valid->passes())
        {
            $admin->nm_petugas = request()->nm_petugas;
            $admin->j_kel_petugas = request()->j_kel_petugas;
            $admin->almt_petugas = request()->almt_petugas;
            $admin->tlp_petugas = request()->tlp_petugas;
            $admin->user = request()->user;
            $admin->password = bcrypt(request()->password);
            $admin->hak_akses = request()->hak_akses;
            $admin->save();

            return redirect('/daftar/petugas');
        }
        else
        {
            return redirect('/daftar/petugas');
        }
    }

    public function edit_petugas($id)
    {
        try
        {
            $data = Petugas::findOrFail($id);

            return view('administrator.pengguna.petugas.edit', ['title' => 'Edit Petugas', 'data' => $data]);
        }
        catch (ModelNotFoundException $e)
        {
            return redirect('/daftar/petugas');
        }
    }

    public function doEdit_petugas($id)
    {
        $valid = Validator::make(request()->all(), [
                'nm_petugas' => 'required',
                'j_kel_petugas' => 'required',
                'almt_petugas' => 'required',
                'tlp_petugas' => 'required'
            ]);

        $petugas = Petugas::findOrFail($id);

            if ($valid->passes()) 
            {
                $petugas->nm_petugas = request()->nm_petugas;
                $petugas->j_kel_petugas = request()->j_kel_petugas;
                $petugas->almt_petugas = request()->almt_petugas;
                $petugas->tlp_petugas = request()->tlp_petugas;
                $petugas->update();

                return redirect('/daftar/petugas');
            }
            else
            {
                return redirect('/daftar/petugas/kzxcmkzxc');
            }
    }

    public function update_petugas()
    {
        $valid = Validator::make(request()->all(), [
                'password_old' => 'required',
                'password' => 'required|confirmed'
            ]);

            if ($valid->passes() && Hash::check(request()->password_old, Auth::user()->password))
            {
                $petugas = Petugas::findOrFail(1);
                $petugas->password = bcrypt(request()->password);
                $petugas->update();

                return redirect('/daftar/petugas');    
            }
            else
            {
                return redirect('/daftar/petugas');
            }
    }

    public function hapus_petugas($id)
    {
        try
        {
            $petugas = Petugas::findOrFail($id);
            $petugas->delete();

            return redirect('/daftar/petugas');
        }
        catch (ModelNotFoundException $e)
        {
            return redirect()->back();
        }
    }
    /* Fungsi CRUD Petugas End */

   	/* Fungsi CRUD Karyawan Start */ 
    public function daftar_karyawan()
    {
        $title = 'Daftar Karyawan';
        $data = Karyawan::get();

        $contents = view('administrator.pengguna.karyawan.index', ['title' => $title, 'no' => 1, 'data' => $data]);

            return Response::make($contents, 200)
                ->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
    }

    public function tambah_karyawan()
    {
        $title = 'Tambah Karyawan';
        $data = Karyawan::get();
        
        $contents = view('administrator.pengguna.karyawan.tambah', ['title' => $title, 'data' => $data]);
        
        return Response::make($contents, 200)
            ->header('Cache-Control','nocache, no-store, max-age=0 must-revalidate');
    }

    public function doTambah_karyawan(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'nm_karyawan' => 'required',
            'status' => 'required',
            'ruang' => 'required',
            'j_kel' => 'required',
            'almt_karyawan' => 'required',
            'tlp_karyawan' => 'required|min:11|numeric'
        ]);

        $karyawan = new Karyawan();

        if ($valid->passes())
        {
            $karyawan->nm_karyawan = request()->nm_karyawan;
            $karyawan->status = request()->status;
            $karyawan->ruang = request()->ruang;
            $karyawan->j_kel = request()->j_kel;
            $karyawan->almt_karyawan = request()->almt_karyawan;
            $karyawan->tlp_karyawan = request()->tlp_karyawan;
            $karyawan->save();

            return redirect('/daftar/karyawan');
        }
        else
        {
            return redirect('/daftar/gagal');
        }
    }

    public function edit_karyawan($id)
    {
        try
        {
            $data = Karyawan::findOrFail($id);

            return view('administrator.pengguna.karyawan.edit', ['title' => 'Edit Karyawan', 'data' => $data]);
        }
        catch (ModelNotFoundException $e)
        {
            return redirect('/daftar/karyawan');
        }
    }

    public function doEdit_karyawan($id)
    {
        $valid = Validator::make(request()->all(), [
                'nm_karyawan' => 'required',
                'status' => 'required',
                'ruang' => 'required',
                'j_kel' => 'required',
                'almt_karyawan' => 'required',
                'tlp_karyawan' => 'required|min:11|numeric'
            ]);

        $karyawan = Karyawan::findOrFail($id);

            if ($valid->passes()) 
            {
                $karyawan->nm_karyawan = request()->nm_karyawan;
                $karyawan->status = request()->status;
                $karyawan->ruang = request()->ruang;
                $karyawan->j_kel = request()->j_kel;
                $karyawan->almt_karyawan = request()->almt_karyawan;
                $karyawan->tlp_karyawan = request()->tlp_karyawan;
                $karyawan->update();

                return redirect('/daftar/karyawan');
            }
            else
            {
                return redirect('/daftar/karyawan/gagal');
            }
    }

    public function hapus_karyawan($id)
    {
        try
        {
            $karyawan = Karyawan::findOrFail($id);
            $karyawan->delete();

            return redirect('/daftar/karyawan');
        }
        catch (ModelNotFoundException $e)
        {
            return redirect()->back();
        }
    }

    /* Fungsi CRUD Karyawan End */
}
