<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Auth;
use Validator;
/* Memanggil Model Peminjaman */
use App\Peminjaman;
/* Memanggil Model Pengembalian */
use App\Pengembalian;
/* Memanggil Model Karyawan */
use App\Karyawan;
/* Memanggil Model Inventaris */
use App\Inventaris;

class TransaksiController extends Controller
{
    function __construct()
    {
        $this->list_karyawan = Karyawan::orderBy('nm_karyawan')
            ->pluck('nm_karyawan', 'kd_karyawan')
            ->toArray();

        $this->list_alat = Inventaris::where('stok', '>', '0')
            ->join('label_alat', 'inventaris.kd_alat', '=', 'label_alat.kd_alat')
            ->pluck('label_alat.label_alat', 'label_alat.kd_alat')
            ->toArray();
    }

    /* Fungsi Peminjaman Start */
    public function daftar_peminjaman()
    {
    	$title = 'Daftar Peminjaman';
        $data = Peminjaman::get();

        $contents = view('transaksi.peminjaman.index', 
            [
                'title' => $title, 
                'no' => 1, 
                'data' => $data
            ]);

        return Response::make($contents, 200)
            ->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate');
    }

    public function tambah_peminjaman()
    {
    	$title = 'Daftar Peminjaman';
        $data = Peminjaman::get();

        $contents = view('transaksi.peminjaman.tambah', 
            [
                'title' => $title, 
                'no' => 1, 
                'data' => $data
            ]);

        return Response::make($contents, 200)
            ->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate');
    }
    /* Fungsi Peminjaman End */

    /* Fungsi Pengembalian Start */

    public function daftar_pengembalian()
    {
    	$title = 'Daftar Pengembalian';
        $data = Pengembalian::get();

        $contents = view('transaksi.pengembalian.index', 
            [
                'title' => $title, 
                'no' => 1, 
                'data' => $data
            ]);

        return Response::make($contents, 200)
            ->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate');
    }

    /* Fungsi Pengembalian End */

}
