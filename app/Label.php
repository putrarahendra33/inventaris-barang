<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    protected $table = 'label_alat';
    protected $primaryKey = 'kd_alat';
    public $timestamps = false;
}
