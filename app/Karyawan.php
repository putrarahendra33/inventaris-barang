<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';
    protected $primaryKey = 'kd_karyawan';

    public $incrementing = false;
    public $timestamps = false;
}
