<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'detail';
    protected $primaryKey = 'kd_alat';
	
	public $incrementing = false;
    public $timestamps = false;
}
