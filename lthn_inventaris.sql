-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 14, 2017 at 07:54 AM
-- Server version: 5.6.21
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lthn_inventaris`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail`
--

CREATE TABLE IF NOT EXISTS `detail` (
  `kd_inventaris` int(11) NOT NULL,
  `label_alat` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE IF NOT EXISTS `inventaris` (
`kd_alat` int(10) NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kd_kategori` int(11) NOT NULL,
  `merk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kd_pembuat` int(11) NOT NULL,
  `thn_buat` date NOT NULL,
  `kondisi_brg` enum('baik','rusak') COLLATE utf8_unicode_ci NOT NULL,
  `status_brg` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
`kd_karyawan` int(10) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nm_karyawan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_daftar` date NOT NULL,
  `j_kel` enum('cowok','cewek') COLLATE utf8_unicode_ci NOT NULL,
  `almt_karyawan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tlp_karyawan` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`kd_kategori` int(10) NOT NULL,
  `jenis_alat` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `label`
--

CREATE TABLE IF NOT EXISTS `label` (
  `label_alat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kd_alat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
`id` int(10) NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(19, '2017_01_11_054018_inventaris', 1),
(20, '2017_01_11_054035_kategori', 1),
(21, '2017_01_11_054049_pembuat', 1),
(22, '2017_01_11_054104_petugas', 1),
(23, '2017_01_11_054119_karyawan', 1),
(24, '2017_01_11_054128_peminjaman', 1),
(25, '2017_01_11_054427_pengembalian', 1),
(26, '2017_01_11_054440_detail', 1),
(27, '2017_01_11_054453_label', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembuat`
--

CREATE TABLE IF NOT EXISTS `pembuat` (
`kd_pembuat` int(10) NOT NULL,
  `nm_pembuat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota_pembuat` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE IF NOT EXISTS `peminjaman` (
  `kd_inventaris` int(11) NOT NULL,
  `kd_karyawan` int(11) NOT NULL,
  `kd_petugas` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian`
--

CREATE TABLE IF NOT EXISTS `pengembalian` (
  `kd_inventaris` int(11) NOT NULL,
  `kd_karyawan` int(11) NOT NULL,
  `kd_petugas` int(11) NOT NULL,
  `tgl_kembali` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
`kd_petugas` int(10) NOT NULL,
  `nm_petugas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `j_kel_petugas` enum('cowok','cewek') COLLATE utf8_unicode_ci NOT NULL,
  `almt_petugas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tlp_petugas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hak_akses` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`kd_petugas`, `nm_petugas`, `j_kel_petugas`, `almt_petugas`, `tlp_petugas`, `user`, `password`, `hak_akses`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'cowok', 'admin', 'admin', 'admin', '$2y$10$jJQJw.Ach3QjrEt1fDvDHucnQcIf7aw/uEQ9mFkRuE5gP.lv925Ui', 1, 'MOHdVh92PBRf34TUJUwINQXSL4d4EFsgYCvvUSUEXrPuIO1O89q7DLP7vNi9', NULL, '2017-01-13 00:45:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail`
--
ALTER TABLE `detail`
 ADD KEY `kd_inventaris` (`kd_inventaris`), ADD KEY `label_alat` (`label_alat`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
 ADD PRIMARY KEY (`kd_alat`), ADD KEY `kd_alat` (`kd_alat`), ADD KEY `model` (`model`), ADD KEY `kd_kategori` (`kd_kategori`), ADD KEY `merk` (`merk`), ADD KEY `kd_pembuat` (`kd_pembuat`), ADD KEY `thn_buat` (`thn_buat`), ADD KEY `kondisi_brg` (`kondisi_brg`), ADD KEY `status_brg` (`status_brg`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
 ADD PRIMARY KEY (`kd_karyawan`), ADD KEY `kd_karyawan` (`kd_karyawan`), ADD KEY `kd_karyawan_2` (`kd_karyawan`), ADD KEY `status` (`status`), ADD KEY `nm_karyawan` (`nm_karyawan`), ADD KEY `ruang` (`ruang`), ADD KEY `tgl_daftar` (`tgl_daftar`), ADD KEY `j_kel` (`j_kel`), ADD KEY `almt_karyawan` (`almt_karyawan`), ADD KEY `tlp_karyawan` (`tlp_karyawan`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`kd_kategori`), ADD KEY `kd_kategori` (`kd_kategori`), ADD KEY `jenis_alat` (`jenis_alat`);

--
-- Indexes for table `label`
--
ALTER TABLE `label`
 ADD PRIMARY KEY (`label_alat`), ADD KEY `label_alat` (`label_alat`), ADD KEY `kd_alat` (`kd_alat`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`), ADD KEY `migration` (`migration`), ADD KEY `batch` (`batch`);

--
-- Indexes for table `pembuat`
--
ALTER TABLE `pembuat`
 ADD PRIMARY KEY (`kd_pembuat`), ADD KEY `kd_pembuat` (`kd_pembuat`), ADD KEY `nm_pembuat` (`nm_pembuat`), ADD KEY `kota_pembuat` (`kota_pembuat`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
 ADD KEY `kd_inventaris` (`kd_inventaris`), ADD KEY `kd_karyawan` (`kd_karyawan`), ADD KEY `kd_petugas` (`kd_petugas`), ADD KEY `tgl_pinjam` (`tgl_pinjam`);

--
-- Indexes for table `pengembalian`
--
ALTER TABLE `pengembalian`
 ADD KEY `kd_inventaris` (`kd_inventaris`), ADD KEY `kd_karyawan` (`kd_karyawan`), ADD KEY `kd_petugas` (`kd_petugas`), ADD KEY `tgl_kembali` (`tgl_kembali`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
 ADD PRIMARY KEY (`kd_petugas`), ADD UNIQUE KEY `petugas_user_unique` (`user`), ADD KEY `kd_petugas` (`kd_petugas`), ADD KEY `nm_petugas` (`nm_petugas`), ADD KEY `j_kel_petugas` (`j_kel_petugas`), ADD KEY `almt_petugas` (`almt_petugas`), ADD KEY `tlp_petugas` (`tlp_petugas`), ADD KEY `user` (`user`), ADD KEY `password` (`password`), ADD KEY `hak_akses` (`hak_akses`), ADD KEY `remember_token` (`remember_token`), ADD KEY `created_at` (`created_at`), ADD KEY `updated_at` (`updated_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
MODIFY `kd_alat` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
MODIFY `kd_karyawan` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `kd_kategori` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `pembuat`
--
ALTER TABLE `pembuat`
MODIFY `kd_pembuat` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
MODIFY `kd_petugas` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail`
--
ALTER TABLE `detail`
ADD CONSTRAINT `detail_ibfk_1` FOREIGN KEY (`label_alat`) REFERENCES `label` (`label_alat`),
ADD CONSTRAINT `detail_ibfk_2` FOREIGN KEY (`kd_inventaris`) REFERENCES `peminjaman` (`kd_inventaris`);

--
-- Constraints for table `inventaris`
--
ALTER TABLE `inventaris`
ADD CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`kd_kategori`) REFERENCES `kategori` (`kd_kategori`),
ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`kd_pembuat`) REFERENCES `pembuat` (`kd_pembuat`);

--
-- Constraints for table `label`
--
ALTER TABLE `label`
ADD CONSTRAINT `label_ibfk_1` FOREIGN KEY (`kd_alat`) REFERENCES `inventaris` (`kd_alat`);

--
-- Constraints for table `peminjaman`
--
ALTER TABLE `peminjaman`
ADD CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`kd_petugas`) REFERENCES `petugas` (`kd_petugas`),
ADD CONSTRAINT `peminjaman_ibfk_2` FOREIGN KEY (`kd_karyawan`) REFERENCES `karyawan` (`kd_karyawan`);

--
-- Constraints for table `pengembalian`
--
ALTER TABLE `pengembalian`
ADD CONSTRAINT `pengembalian_ibfk_1` FOREIGN KEY (`kd_petugas`) REFERENCES `petugas` (`kd_petugas`),
ADD CONSTRAINT `pengembalian_ibfk_2` FOREIGN KEY (`kd_karyawan`) REFERENCES `karyawan` (`kd_karyawan`),
ADD CONSTRAINT `pengembalian_ibfk_3` FOREIGN KEY (`kd_inventaris`) REFERENCES `peminjaman` (`kd_inventaris`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
